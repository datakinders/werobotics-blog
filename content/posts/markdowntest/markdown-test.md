---
title: "Markdown Test" 
date: 2020-09-08T18:49:39-04:00
draft: false
categories:
    - "category1"
tags:
    - "tag1"
    - "tag2"
---

This is a test of the Markdown blog post functionality. 
In particular, this is a test of the equation/math functionality. 

\begin{align}
\frac{\partial Q}{\partial t} = \frac{\partial s}{\partial t} \\
\frac{\partial P}{\partial x} = \frac{\partial V}{\partial T}
\end{align}

There is sentence with math $y_{t} = a*y_{t-1} + \epsilon \sim N(0, \sigma^2)$.


\begin{equation}
x^n + y^n = z^n 
\end{equation}

There is the same math again. 

$$
x_t = a*x_{t-1} + \epsilon \sim N(0,1)
$$

some more math.



2020-09-08 
