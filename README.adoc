= README

This repository contains a blog for the Datakind WeRobotics 
building detector project. Contributors can add and change posts/content
on our hugo blog. The blog itself is hosted on Gitlab pages at 
https://datakinders.gitlab.io/werobotics-blog/


== Installation 

1. Before installing this repository, please make sure that you have 
the Go language installed. Any recent go version should be fine, 
at least version 1.11 or above. 

2. Next install the Hugo blog package following the instructions provided
in the link:https://gohugo.io/getting-started/installing/[Hugo Installation Guide].

3. Once you have both of these dependencies installed, you can clone the repository 
using 

----
git clone git@gitlab.com:datakinders/werobotics-blog.git
----

4. Next navigate to that newly installed `werobotics-blog` directory using `cd werobotics-blog`.

[NOTE]
====
Before running `Hugo` you must install the `beautifulhugo` theme as per the 
step below. `Hugo` will not start unless the theme is installed. 
====

5. Before starting hugo, install the theme into the `theme` directory. From the 
root folder of the repository, type:

----
git clone https://gitlab.com/00krishna/beautifulhugo.git themes/beautifulhugo
----

Now you are ready to start the blog on a local server. 

== Starting Hugo blog on a local server

Navigate to the root directory of the hugo blog and type the following command.

----
hugo -v -d ./build server
----

This will start a local server on the computer. The output of the command will 
direct you to a link to paste into your browser. The link should look like http://localhost:1313/werobotics-blog/

Now you should be able to navigate around the blog as it if were any other webpage. 

All of the live files for the local website are located in the `werobotics-blog/build` folder. 
Hugo will copy files from the `werobotics-blog/content/posts` folder to the `werobotics-blog/build`
folder when the server starts up. 

== Writing New Posts

Contributors may write new posts in Markdown or Asciidoc formats. To create a new post skeleton, `Hugo` 
provides some useful helpers. Note that all posts are stored in the `content/posts` folder. There is 
one folder per post. 

To create a new post skeleton, you can use the following code, depending on whether you plan to write 
in markdown or asciidoc.

----
hugo new content/posts/mynewpost/mypost.md
----

or

----
hugo new content/posts/mynewpost/mypost.adoc 
----

This will create a new post skeleton. The contributor can fill in the frontmatter template, to 
indicate the title of the post, tags, etc. Note that post have the default setting of `draft: true`. 
Drafts are not displayed on the live gitlab pages blog. To display drafts on the local server, you can include the 
`-buildDrafts` flag like:

----
hugo -v -D -d ./build server
----

To include images in a post, the contributor has to create a new folder in the `content/posts/images` folder. 
All the images for the post should be stored in the newly created folder. Then in the markdown or asciidoc 
post itself, all images should reference the location `../../images/mypost/`. For example, one of the 
test posts has a link to an image in `image::../../images/secondpost/water-image.jpg[]`, for an asciidoc post. 

